import React,{useContext, useState, useEffect, useRef} from 'react';
import Header from '../../components/Header';
import Form from '../../components/Form';
import Character from '../character';
import Oponent from '../../components/Oponent';
import User from '../../components/User';
import {
  UserStatsProvider,
  OponentStatProvider,
  CharStats,
  GameApp,
  GameState,
} from '../../context';
import Homepage from '../homepage';
import { useLocalstorage } from "rooks";


const Game = () => {
  const [name, addName] = useLocalstorage('name', '' );
  const {str, hp, speed, addStr} = useContext(CharStats);
  const {isName, setName} = useContext(GameState);
  const refPo = useRef(null);
  const refD = useRef(null);
  const refL = useRef(null);
  const refPu = useRef(null);
  const refG = useRef(null);

// useEffect (()=>{
//   console.log(isName)
//   // const getName = localStorage.getItem('name');
//   // if (getName.length > 0) (setName(true))
// },[isName])

useEffect (()=>{
    console.log(localStorage.getItem(name));
    // localStorage.setItem("name", name);
    // const addName = localStorage.getItem('name');
    (!name) ? localStorage.setItem("name",name) : localStorage.getItem(name)
  },[name])

  // useEffect(()=>{addName(localStorage.getItem("name"))},[name])

return (
  <div>

    {name ?
       (
      <div>
      <Header {...{refPo, refD, refL, refPu, refG}} />
      <div className="col-12 destination" ref={refPo}>Polana</div>
      <div className="col-12 destination" ref={refD}>Las</div>
      <div className="col-12 destination" ref={refL}>Dżungla</div>
      <div className="col-12 destination" ref={refPu}>Pustynia</div>
      <div className="col-12 destination" ref={refG}>Góry</div>
      <div className="game__wrapper">
        <UserStatsProvider>
          <User />
        </UserStatsProvider>
        <OponentStatProvider>
          <Oponent alive />
        </OponentStatProvider>
      </div>
      </div>
    ) : (
      <Form />

    )}
  </div>
 );
};


export default Game;
