import Style from './style.css';
import {useContext, useEffect, useState} from 'react';
import {CharStats, GameState} from '../../context'
import TableRow from "../../components/TableRow";
import ProgressBar from '../../components/ProgressBar';
import { useLocalstorage } from "rooks";
import Form from '../../components/Form';



const Character = () => {

  // function getValueFromLocalStorage() {
  //
  //        if (typeof localStorage === "undefined") {
  //            return name;
  //        }
  //        var storedValue = localStorage.getItem(name) || "null";
  //        try {
  //            return JSON.parse(storedValue);
  //        }
  //        catch (error) {
  //            console.log('o')
  //        }
  //        return storedValue;
  //    }

  const [name, addName] =  useLocalstorage('name','');
  localStorage.setItem("name", name);
  const { str, hp, speed, addStr, dmg, lvl } = useContext(CharStats);
  const myStats = {name, str, hp, speed, dmg, lvl};
  useEffect (()=>{
      console.log(localStorage.getItem(name));
      // localStorage.setItem("name", name);
      // const addName = localStorage.getItem('name');
      (!name) ? localStorage.setItem("name",name) : localStorage.getItem(name)
    },[name])

    // useEffect(()=>{addName(localStorage.getItem("name"))},[name])
  return (
    <div>
      {!name ? (
    <Form />

  ) : (

    <div className="col-6">
     <TableRow {...myStats} />
     <ProgressBar />
     <button value="" onClick={(e) => localStorage.removeItem("name")}>
       Kliknij i usuń localStorage, odśwież stronę po kliknięciu
   </button>
    </div>


  )}
</div>
);
};

export default Character
