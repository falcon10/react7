import React,{useContext, useState, useEffect} from 'react';
import Form from '../../components/Form';
import User from '../../components/User';
import Oponent from '../../components/Oponent';
import { useLocalstorage } from "rooks";
import {
  UserStatsProvider,
  OponentStatProvider,
  CharStats,
  GameApp,
  GameState,
} from '../../context';
import Character from '../homepage';


const Homepage = () => {
  const [name, addName] = useLocalstorage('name', '' );
  const {str, hp, speed, addStr} = useContext(CharStats);
  localStorage.setItem("name", name);
  const {isName, setName} = useContext(GameState);

//   useEffect (()=>{
//   // console.log(isName)
//   // const getName = localStorage.getItem('name');
//   // if (getName.length > 0) (setName(true))
// },[isName])

useEffect (()=>{
    console.log(localStorage.getItem(name));
    // localStorage.setItem("name", name);
    // const addName = localStorage.getItem('name');
    (!name) ? localStorage.setItem("name",name)  : localStorage.getItem("name")
  },[name])

  // useEffect(()=>{addName(localStorage.getItem("name"))},[name])

return (
  <div>
    {name ? (
      <div className="game__wrapper">
        <UserStatsProvider>
          <User />
        </UserStatsProvider>

        <OponentStatProvider>
          <Oponent alive />
        </OponentStatProvider>
      </div>

    ) : (
      <Form />

    )}
  </div>
 );
};

export default Homepage;
