import logo from './logo.svg';
import './App.css';
import Homepage from './pages/homepage';
import {GameApp} from './context';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import Header from './components/Header';
import Form from './components/Form';
import User from './components/User';
import Character from './pages/character';
import Game from './pages/game';

function App() {
  return (
    <div>
    <button onClick={event =>  window.location.href='/homepage'}>Start the game</button>
    <p></p>

    <Router>
    <GameApp>
         <li>
            <Link to="/homepage">Home</Link>
         </li>
         <li>
            <Link to="/character">Postać</Link>
         </li>
         <li>
            <Link to="/game">Świat gry</Link>
         </li>
     <Routes>
      <Route exact path='/homepage' element={< Homepage />}> </Route>
      <Route exact path='/character' element={< Character />}> </Route>
      <Route exact path='/game' element={< Game />}> </Route>
      </Routes>
      </GameApp>
      </Router>
    </div>
  );
}

export default App;
