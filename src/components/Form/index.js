import React, {useContext, useState, useEffect } from "react";
import { useLocalstorage } from "rooks";
import {GameState} from '../../context'

const Form = () => {

  const [name, addName] = useLocalstorage("name",null);
  const {isName, setName} = useContext(GameState);
  localStorage.getItem('name')
  localStorage.setItem('name',name);

  const handleSubmit = (e) => {
    e.preventDefault();
    setName(true);
    localStorage.getItem('name')
    localStorage.setItem('name',name);
}



 return (
   <form onSubmit = {e => handleSubmit (e)}>
   <label> Wprowadź imię i odśwież stronę:</label>
      <input name="name" type="text" value={undefined} onChange={(e)=>addName(e.target.value)}/>
      <button value="" type="submit">
      Potwierdź klikając po odświeżeniu strony
    </button>
   </form>

 )
}

export default Form
