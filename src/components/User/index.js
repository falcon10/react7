import Style from './style.css';
import {useContext, useEffect, useState} from 'react';
import {CharStats} from '../../context'
import TableRow from "../../components/TableRow";
import ProgressBar from '../../components/ProgressBar';
import { useLocalstorage } from "rooks";



const User = () => {
  const [name, addName] = useLocalstorage('name');
  localStorage.setItem("name", name);
  const { str, hp, speed, addStr, dmg, lvl } = useContext(CharStats);
  const myStats = {name, str, hp, speed, dmg, lvl};
  useEffect(()=>{addName(localStorage.getItem("name"))},[name])
  return (
    <div className="col-6">
     <TableRow {...myStats} />
     <ProgressBar />
     <button value="" onClick={(e) => localStorage.removeItem("name")}>
       Kliknij i usuń localStorage, odśwież stronę po kliknięciu
   </button>
    </div>
  )
}

export default User;
