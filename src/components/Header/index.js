import React from 'react';

const scrollToRef = ref => window.scrollTo(0, ref.current.offsetTop);

const Header = ({refPo, refD, refL, refPu, refG}) => {

const navList = [
  {'name':'Polana', 'ref': refPo},
  {'name':'Las', 'ref': refL},
  {'name':'Dżungla', 'ref': refD},
  {'name':'Pustynia', 'ref': refPu},
  {'name':'Góry', 'ref': refG},
]

  return (

    <div>
       <ul>
        {
         navList.map(el => {
           return <li key={el.name} onClick={()=>scrollToRef(el.ref)}>{el.name}</li>
         })
       }
       </ul>
    </div>
  );
}
export default Header;
